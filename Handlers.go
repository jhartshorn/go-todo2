package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"time"
)

func Index(w http.ResponseWriter, r *http.Request) {
	//Front Page
	p := &Page{Title: "GoDo"}
	renderTemplate(w, "head", p)
	renderTemplate(w, "menu", p)
	renderTemplate(w, "welcome", p)
	renderTemplate(w, "foot", p)

}

func TodoIndex(w http.ResponseWriter, r *http.Request) {
	//The List of all todos
	fmt.Println("Handler:TodoIndex")
	SERVER := "127.0.0.1:27017"
	DB := "test"
	C := "Todos"

	c, err, session := openDB(SERVER, DB, C)
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()
	result := Todo{}
	iter := c.Find(nil).Sort("-due").Iter()

	p := &Page{Title: ""}
	renderTemplate(w, "head", p)
	renderTemplate(w, "menu", p)
	renderTemplate(w, "tlist_top", p)

	for iter.Next(&result) {
		t := &Task_part{Name: result.Name}
		renderTask_part(w, "task_part", t)

	}
	renderTemplate(w, "tlist_bot", p)
	renderTemplate(w, "foot", p)
}

func TodoShow(w http.ResponseWriter, r *http.Request) {
	SERVER := "127.0.0.1:27017"
	DB := "test"
	C := "Todos"

	vars := mux.Vars(r)
	name := vars["Name"]
	p := &Page{Title: ""}
	renderTemplate(w, "head", p)
	renderTemplate(w, "menu", p)
	fmt.Fprintf(w, "<ul><a href=\"/todos\"><img src=\"/images/back.png\" width=\"20\" height=\"20\"></a><h1>%s</h1><p>%s</p><p>Due:%s</p>", findTodo("name", name, SERVER, DB, C).Name, findTodo("name", name, SERVER, DB, C).Note, findTodo("name", name, SERVER, DB, C).Due)
	renderTemplate(w, "foot", p)
}

func TodoDelete(w http.ResponseWriter, r *http.Request) {
	SERVER := "127.0.0.1:27017"
	DB := "test"
	C := "Todos"

	vars := mux.Vars(r)
	name := vars["Name"]

	removeTodo("name", name, SERVER, DB, C)
	http.Redirect(w, r, "/todos/", http.StatusFound)
}

func TodoNew(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Handler:TodoNew")
	p := &Page{Title: "New"}
	renderTemplate(w, "head", p)
	renderTemplate(w, "menu", p)
	renderTemplate(w, "new", p)
	renderTemplate(w, "foot", p)
}

func TodoAdd(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Handler:TodoAdd")
	name := r.FormValue("name")
	note := r.FormValue("note")
	due := time.Now()

	SERVER := "127.0.0.1:27017"
	DB := "test"
	C := "Todos"
	insertTodo(name, false, due, note, SERVER, DB, C)

	http.Redirect(w, r, "/todos/", http.StatusFound)

}
