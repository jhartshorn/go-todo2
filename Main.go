package main

import (
	"log"
	"net/http"
)

func main() {

	router := NewRouter()
	router.PathPrefix("/images/").Handler(http.StripPrefix("/images/", http.FileServer(http.Dir("/home/james/sync/devel/langs/go/src/todo2/images/"))))

	log.Fatal(http.ListenAndServe(":8080", router))
}
