//Database handling code
package main

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"time"
)

func openDB(server string, dbName string, colName string) (c *mgo.Collection, err error, session *mgo.Session) {
	session, err = mgo.Dial(server)
	if err != nil {
		panic(err)
	}
	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)
	c = session.DB(dbName).C(colName)
	return
}

func insertTodo(name string, completed bool, due time.Time, note string, SERVER string, DB string, C string) {
	c, err, session := openDB(SERVER, DB, C)

	// type Todo struct {
	// 	Name      string    `json:"name"`
	// 	Completed bool      `json:"completed"`
	// 	Due       time.Time `json:"due"`
	// 	Note      string    `json:"note"`
	// }

	err = c.Insert(&Todo{name, completed, due, note})
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

}

func findTodo(field string, value string, SERVER string, DB string, C string) (result Todo) {
	fmt.Println("Entered findTodo func")
	c, err, session := openDB(SERVER, DB, C)
	result = Todo{}
	err = c.Find(bson.M{field: value}).One(&result)
	if err != nil {
		fmt.Println("Find Error findTodo")
		log.Fatal(err)
	}
	defer session.Close()
	return

}

func removeTodo(field string, value string, SERVER string, DB string, C string) (result Todo) {
	fmt.Println("Entered removeTodo func")
	c, err, session := openDB(SERVER, DB, C)
	result = Todo{}
	err = c.Remove(bson.M{field: value})
	if err != nil {
		fmt.Println("Remove Error removeTodo")
		log.Fatal(err)
	}
	defer session.Close()
	return

}
