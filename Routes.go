package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},
	Route{
		"TodoIndex",
		"GET",
		"/todos",
		TodoIndex,
	},
	Route{
		"TodoShow",
		"GET",
		"/todos/{Name}",
		TodoShow,
	},
	Route{
		"TodoNew",
		"GET",
		"/new",
		TodoNew,
	},
	Route{
		"TodoAdd",
		"POST",
		"/new",
		TodoAdd,
	},
	Route{
		"TodoDelete",
		"GET",
		"/deltodos/{Name}",
		TodoDelete,
	},
}
